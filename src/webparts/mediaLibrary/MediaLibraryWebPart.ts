import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { IPropertyPaneConfiguration, PropertyPaneTextField, PropertyPaneSlider } from '@microsoft/sp-property-pane';

import * as strings from 'MediaLibraryWebPartStrings';
import MediaLibrary from './components/MediaLibrary';
import { IMediaLibraryProps } from './components/IMediaLibraryProps';

export interface IMediaLibraryWebPartProps {
  wbTitle: string;
  listName: string;
  seeAllLink: string;
  webUrl: string;
  numberOfItemsToDisplay: number;
}

export default class MediaLibraryWebPart extends BaseClientSideWebPart<IMediaLibraryWebPartProps> {
  public render(): void {
    const element: React.ReactElement<IMediaLibraryProps> = React.createElement(MediaLibrary, {
      wbTitle: this.properties.wbTitle,
      listName: this.properties.listName ? this.properties.listName : 'MediaLibrary',
      seeAllLink: this.properties.seeAllLink,
      webUrl: this.properties.webUrl,
      numberOfItemsToDisplay: this.properties.numberOfItemsToDisplay
    });

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('wbTitle', {
                  label: 'Web Part Title'
                }),
                PropertyPaneTextField('listName', {
                  label: 'SharePoint List Name',
                  value: 'MediaLibrary'
                }),
                PropertyPaneTextField('seeAllLink', {
                  label: '“See All” Navigation Link'
                }),
                PropertyPaneSlider('numberOfItemsToDisplay', {
                  label: 'Number of Items to Be Displayed',
                  min: 1,
                  max: 21,
                  showValue: true,
                  step: 1,
                  value: 4
                }),
                PropertyPaneTextField('wbTitle', {
                  label: 'Web Part Title'
                }),
                PropertyPaneTextField('webUrl', {
                  label: 'Absolute Site URL'
                })
              ]
            }
          ]
        }
      ]
    };
  }
}

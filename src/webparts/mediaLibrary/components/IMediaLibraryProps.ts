export interface IMediaLibraryProps {
  wbTitle: string;
  listName: string;
  seeAllLink: string;
  numberOfItemsToDisplay: number;
  webUrl: string;
}

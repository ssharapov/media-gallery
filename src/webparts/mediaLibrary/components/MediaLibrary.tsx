import * as React from 'react';
// import styles from './MediaLibrary.module.scss';
import { IMediaLibraryProps } from './IMediaLibraryProps';
import { IImage } from '../constants';
import { Service } from '../service';
import ImageGallery from 'react-image-gallery';
import 'react-image-gallery/styles/css/image-gallery.css';

export interface IState {
  images: IImage[];
  loaded: Boolean;
}
export default class MediaLibrary extends React.Component<IMediaLibraryProps, {}> {
  public state = { state: [], loaded: false };
  public service = new Service();
  public componentDidMount() {
    this.service.get(this.props.listName, this.props.webUrl, this.props.numberOfItemsToDisplay).then(images => {
      this.setState({ images, loaded: true });
    });
  }

  public render(): React.ReactElement<IMediaLibraryProps> {
    const images = [
      {
        original: 'http://lorempixel.com/1000/600/nature/1/',
        thumbnail: 'http://lorempixel.com/250/150/nature/1/'
      },
      {
        original: 'http://lorempixel.com/1000/600/nature/2/',
        thumbnail: 'http://lorempixel.com/250/150/nature/2/'
      },
      {
        original: 'http://lorempixel.com/1000/600/nature/3/',
        thumbnail: 'http://lorempixel.com/250/150/nature/3/'
      }
    ];

    return <ImageGallery items={images} />;
  }
}

declare interface IMediaLibraryWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'MediaLibraryWebPartStrings' {
  const strings: IMediaLibraryWebPartStrings;
  export = strings;
}

export interface IImage {
  imageUrl: string;
  title: string;
  videoUrl: string;
}

export enum Fields {
  Title = 'Title',
  Url = 'Url',
  Attachement = 'From'
}

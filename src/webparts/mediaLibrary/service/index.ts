import { IImage, Fields } from '../constants';
import { sp, Web } from '@pnp/sp';
import { AttachmentFile, AttachmentFiles, AttachmentFileInfo } from '@pnp/sp/src/attachmentfiles';

export interface IService {
  get(sourceList: string, webUrl: string, numberOfItemsToDisplay: number): Promise<IImage[]>;
}

export class Service implements IService {
  public get(sourceList: string, webUrl: string, numberOfItemsToDisplay: number): Promise<IImage[]> {
    let web = webUrl ? new Web(webUrl) : sp.web;

    return (
      web.lists
        .getByTitle(sourceList)
        .items // .select('Id,Title,Attachments,AttachmentFiles')
        // .expand('AttachmentFiles')
        // .filter('Attachments eq 1')
        .select(Fields.Title, Fields.Url, 'AttachmentFiles')
        .expand('AttachmentFiles')
        // .filter('Attachments eq 1')
        // .orderBy('Created', false)
        // .top(numberOfItemsToDisplay)
        .get()
        .then(listItems => {
          console.log(listItems);
          let images = [];
          listItems.forEach(item => {
            const image: IImage = {
              imageUrl: item['AttachmentFiles'][0]['ServerRelativeUrl'],
              title: item[Fields.Title],
              videoUrl: item[Fields.Url]
            };
            images.push(image);
          });
          console.log(images);
          return images;
        })
    );
  }
}
